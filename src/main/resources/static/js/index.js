$(document).ready(function () {
    let splide = new Splide('.splide', {
        type: 'loop',
        perPage: 3,
        focus: 'center',
    });

    splide.mount();

    $(document).on("submit", "#playlistForm", function (e) {
        e.preventDefault()

        $(this).ajaxSubmit({
            target: '#createPlaylistModal',
        });
    })

})