let stomp = null

$(document).ready(function () {
    const prototypeComment = $('#playlistCommentListPrototype')
    const commentsContainer = $('#playlistCommentList')
    const playlistId = commentsContainer.data('playlist-id')

    connectWebSocket()

    $(document).on("submit", "#playlistCommentForm", function (e) {
        e.preventDefault()

        $(this).ajaxSubmit({
            target: '#createPlaylistCommentContainer',
        });
    })

    function connectWebSocket() {
        const socket = new SockJS('/spotify-websocket')

        stomp = Stomp.over(socket);
        stomp.connect({}, function (frame) {
            console.log('Connected: ' + frame);
            stomp.subscribe('/playlists/' + playlistId + '/comments', function (message) {
                console.log('New Message: ' + message);
                showComment(JSON.parse(message.body));
            });
        });
    }

    function showComment(body) {
        let comment = prototypeComment.clone()
        comment.find('.playlist-comment-username').text(body.createdBy ?? 'Author Unkonwn')
        comment.find('.playlist-comment-content').text(body.content)
        comment.removeClass('d-none')
        comment.removeAttr('id')

        commentsContainer.prepend(comment);
    }
})