package pl.softivo.spotify.web.form;

import jakarta.validation.constraints.NotBlank;

public class PlaylistForm {

    @NotBlank
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
