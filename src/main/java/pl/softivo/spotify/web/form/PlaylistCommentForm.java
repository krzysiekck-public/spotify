package pl.softivo.spotify.web.form;

import jakarta.validation.constraints.NotBlank;

public class PlaylistCommentForm {

    @NotBlank
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
