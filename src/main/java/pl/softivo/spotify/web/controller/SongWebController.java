package pl.softivo.spotify.web.controller;

import jakarta.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import pl.softivo.spotify.util.Spotify;

@Controller
public class SongWebController {

    @Resource(name = "spotify")
    Spotify spotify;

    @GetMapping("/songs")
    public String list(Model model) {
        model.addAttribute("songs", spotify.getSongs());
        return "song/list";
    }

}
