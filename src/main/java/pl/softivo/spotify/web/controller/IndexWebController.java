package pl.softivo.spotify.web.controller;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.softivo.spotify.repository.PlaylistRepository;
import pl.softivo.spotify.web.form.PlaylistForm;

import java.util.List;

@Controller
public class IndexWebController {

    PlaylistRepository repository;

    public IndexWebController(PlaylistRepository repository) {
        this.repository = repository;
    }

    @RequestMapping("/")
    public String index(Model model) {
        Pageable pageable = PageRequest.ofSize(20);
        model.addAttribute("form", new PlaylistForm());
        model.addAttribute("playlists", repository.findAll(pageable));
        return "index";
    }

}
