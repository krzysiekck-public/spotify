package pl.softivo.spotify.web.controller;

import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.softivo.spotify.entity.Playlist;
import pl.softivo.spotify.entity.PlaylistComment;
import pl.softivo.spotify.repository.PlaylistCommentRepository;
import pl.softivo.spotify.repository.PlaylistRepository;
import pl.softivo.spotify.web.form.PlaylistCommentForm;
import pl.softivo.spotify.web.form.PlaylistForm;
import pl.softivo.spotify.websocket.MessageBroker;
import pl.softivo.spotify.websocket.message.PlaylistCommentMessage;

import java.security.Principal;

@Controller
public class PlaylistWebController {

    private final PlaylistRepository playlistRepository;
    private final PlaylistCommentRepository commentRepository;
    private MessageBroker broker;

    public PlaylistWebController(MessageBroker broker,
                                 PlaylistRepository repository,
                                 PlaylistCommentRepository commentRepository) {
        this.broker = broker;
        this.playlistRepository = repository;
        this.commentRepository = commentRepository;
    }

    @PostMapping("/playlists")
    public String create(@Valid @ModelAttribute PlaylistForm form, BindingResult result, Model model, Principal principal) {
        if (!result.hasErrors()) {
            Playlist playlist = new Playlist(form.getTitle(), principal != null ? principal.getName() : null);
            playlistRepository.save(playlist);
            form = new PlaylistForm();
        }

        model.addAttribute("form", form);
        return "playlist/fragment/modal-create";
    }

    @GetMapping("/playlists/{id}")
    public String view(@PathVariable("id") Playlist playlist, Model model) {
        model.addAttribute("playlist", playlist);
        model.addAttribute("form", new PlaylistCommentForm());
        return "playlist/view";
    }

    @PostMapping("/playlists/{id}/comments")
    public String createComment(@PathVariable("id") Playlist playlist,
                                @Valid @ModelAttribute PlaylistCommentForm form,
                                Principal principal,
                                BindingResult result,
                                Model model) {
        if (!result.hasErrors()) {
            PlaylistComment playlistComment = new PlaylistComment(
                    playlist, form.getContent(), principal != null ? principal.getName() : null);
            commentRepository.save(playlistComment);
            form = new PlaylistCommentForm();
            PlaylistCommentMessage message = new PlaylistCommentMessage(playlistComment);

            broker.sendMessageTo(message, String.format("/playlists/%d/comments", playlist.getId()));
        }

        model.addAttribute("playlist", playlist);
        model.addAttribute("form", form);
        return "playlist/fragment/comment-create";
    }

}
