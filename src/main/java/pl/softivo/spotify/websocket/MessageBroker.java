package pl.softivo.spotify.websocket;

import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class MessageBroker {

    final SimpMessagingTemplate template;

    public MessageBroker(SimpMessagingTemplate template) {
        this.template = template;
    }

    public void sendMessageTo(Object message, String destination) {
        template.convertAndSend(destination, message);
    }

}
