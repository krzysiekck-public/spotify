package pl.softivo.spotify.websocket.message;

import pl.softivo.spotify.entity.PlaylistComment;

public class PlaylistCommentMessage {

    private String content;

    private String createdBy;

    public PlaylistCommentMessage(PlaylistComment comment) {
        this.content = comment.getContent();
        this.createdBy = comment.getCreatedBy();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
