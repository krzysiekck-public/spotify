package pl.softivo.spotify.websocket.controller;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import pl.softivo.spotify.entity.PlaylistComment;
import pl.softivo.spotify.entity.Song;
import pl.softivo.spotify.websocket.message.PlaylistCommentMessage;

@Controller
public class PlaylistWebSocket {

    @MessageMapping("/playlists/{id}/comments")
    @SendTo("/playlists/{id}/comments")
    public PlaylistCommentMessage song(PlaylistComment comment) throws Exception {
        return new PlaylistCommentMessage(comment);
    }

}
