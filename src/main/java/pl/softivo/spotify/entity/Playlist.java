package pl.softivo.spotify.entity;

import jakarta.persistence.*;

import java.util.Date;
import java.util.List;

@Entity
public class Playlist {

    private @Id @GeneratedValue Long id;

    private String title;

    private String createdBy;

    @OneToMany(mappedBy = "playlist", fetch = FetchType.LAZY)
    private List<PlaylistComment> comments;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt = new Date();

    public Playlist() {}

    public Playlist(String title, String createdBy) {
        this.title = title;
        this.createdBy = createdBy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public List<PlaylistComment> getComments() {
        return comments;
    }

    public void setComments(List<PlaylistComment> comments) {
        this.comments = comments;
    }

    public void addComment(PlaylistComment comment) {
        this.comments.add(comment);
    }

    @Override
    public String toString() {
        return "Playlist{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdAt='" + createdAt + '\'' +
                '}';
    }
}
