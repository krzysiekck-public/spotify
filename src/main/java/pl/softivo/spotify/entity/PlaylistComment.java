package pl.softivo.spotify.entity;

import jakarta.persistence.*;

import java.util.Date;

@Entity
public class PlaylistComment {

    private @Id @GeneratedValue Long id;

    @Column(columnDefinition = "TEXT", nullable = false)
    private String content;

    @ManyToOne
    @JoinColumn(name = "playlist_id")
    private Playlist playlist;

    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt = new Date();

    public PlaylistComment() {}

    public PlaylistComment(Playlist playlist, String content, String createdBy) {
        this.playlist = playlist;
        this.content = content;
        this.createdBy = createdBy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Playlist getPlaylist() {
        return playlist;
    }

    public void setPlaylist(Playlist playlist) {
        this.playlist = playlist;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Playlist{" +
                "id=" + id +
                ", createdBy='" + createdBy + '\'' +
                ", createdAt='" + createdAt + '\'' +
                '}';
    }
}
