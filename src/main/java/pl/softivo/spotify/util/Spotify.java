package pl.softivo.spotify.util;

import org.apache.hc.core5.http.ParseException;
import se.michaelthelin.spotify.exceptions.SpotifyWebApiException;
import se.michaelthelin.spotify.model_objects.specification.TrackSimplified;
import se.michaelthelin.spotify.requests.data.browse.GetRecommendationsRequest;

import java.io.IOException;


public class Spotify {

    private final se.michaelthelin.spotify.SpotifyApi client;

    public Spotify(String clientId, String clientSecret, String accessToken, String refreshToken) {
        this.client = new se.michaelthelin.spotify.SpotifyApi.Builder()
                .setClientId(clientId)
                .setClientSecret(clientSecret)
                .setAccessToken(accessToken)
                .setRefreshToken(refreshToken)
                .build();
    }

    public TrackSimplified[] getSongs() {
        TrackSimplified[] tracks = {};
        GetRecommendationsRequest request = client.getRecommendations().build();

        try {
            tracks = request.execute().getTracks();
        } catch (IOException | RuntimeException | SpotifyWebApiException | ParseException e) {
        }

        return tracks;
    }

}

