package pl.softivo.spotify.util;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.security.core.Authentication;


@ConfigurationProperties("spotify")
public class SpotifyConfig {

    private String spotifyId;
    private String spotifySecret;

    @Bean
    @RequestScope
    public Spotify spotify(OAuth2AuthorizedClientService clientService) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String accessToken = null;
        String refreshToken = null;

        if (authentication.getClass().isAssignableFrom(OAuth2AuthenticationToken.class)) {
            OAuth2AuthenticationToken oauthToken = (OAuth2AuthenticationToken) authentication;
            String clientRegistrationId = oauthToken.getAuthorizedClientRegistrationId();

            if (clientRegistrationId.equals("spotify")) {
                OAuth2AuthorizedClient client = clientService.loadAuthorizedClient(
                        clientRegistrationId, oauthToken.getName());

                if (client != null) {
                    accessToken = client.getAccessToken().getTokenValue();
                    refreshToken = client.getRefreshToken().getTokenValue();
                }
            }
        }

        return new Spotify(spotifyId, spotifySecret, accessToken, refreshToken);
    }

    public String getSpotifyId() {
        return spotifyId;
    }

    public void setSpotifyId(String spotifyId) {
        this.spotifyId = spotifyId;
    }

    public String getSpotifySecret() {
        return spotifySecret;
    }

    public void setSpotifySecret(String spotifySecret) {
        this.spotifySecret = spotifySecret;
    }
}

