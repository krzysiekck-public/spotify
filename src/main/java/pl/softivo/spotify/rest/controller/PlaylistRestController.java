package pl.softivo.spotify.rest.controller;

import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;
import pl.softivo.spotify.entity.Playlist;
import pl.softivo.spotify.entity.PlaylistComment;
import pl.softivo.spotify.repository.PlaylistCommentRepository;
import pl.softivo.spotify.repository.PlaylistRepository;
import pl.softivo.spotify.rest.dto.PlaylistCommentInput;
import pl.softivo.spotify.rest.dto.PlaylistCommentOutput;
import pl.softivo.spotify.websocket.MessageBroker;
import pl.softivo.spotify.websocket.message.PlaylistCommentMessage;

import java.security.Principal;

@RestController
@RequestMapping("/api")
public class PlaylistRestController {

    private final PlaylistRepository playlistRepository;
    private final PlaylistCommentRepository commentRepository;
    private final MessageBroker broker;

    public PlaylistRestController(PlaylistRepository playlistRepository,
                                  PlaylistCommentRepository commentRepository,
                                  MessageBroker broker) {
        this.playlistRepository = playlistRepository;
        this.commentRepository = commentRepository;
        this.broker = broker;
    }

    @PostMapping("/playlists/{id}/comments")
    private PlaylistCommentOutput createComment(@PathVariable("id") Playlist playlist,
                                                @Valid @RequestBody PlaylistCommentInput playlistCommentInput,
                                                Principal principal) {
        PlaylistComment comment = new PlaylistComment(playlist, playlistCommentInput.getContent(), principal != null ? principal.getName() : null);
        commentRepository.save(comment);
        broker.sendMessageTo(new PlaylistCommentMessage(comment), String.format("/playlists/%d/comments", playlist.getId()));

        return new PlaylistCommentOutput(comment);
    }

}
