package pl.softivo.spotify.rest.dto;

import jakarta.validation.constraints.NotBlank;

public class PlaylistCommentInput {

    @NotBlank
    private String content;

    public PlaylistCommentInput() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
