package pl.softivo.spotify.rest.dto;

import pl.softivo.spotify.entity.PlaylistComment;

public class PlaylistCommentOutput {

    private String content;

    private String createdBy;

    public PlaylistCommentOutput() {
    }

    public PlaylistCommentOutput(PlaylistComment comment) {
        this.content = comment.getContent();
        this.createdBy = comment.getCreatedBy();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
