package pl.softivo.spotify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import pl.softivo.spotify.util.SpotifyConfig;

@SpringBootApplication
@EnableConfigurationProperties(SpotifyConfig.class)
public class SpotifyApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpotifyApplication.class, args);
    }

}
