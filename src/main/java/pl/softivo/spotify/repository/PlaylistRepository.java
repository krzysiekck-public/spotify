package pl.softivo.spotify.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.softivo.spotify.entity.Playlist;

@Repository
public interface PlaylistRepository extends JpaRepository<Playlist, Long> { }
