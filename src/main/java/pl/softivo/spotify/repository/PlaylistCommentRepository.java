package pl.softivo.spotify.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.softivo.spotify.entity.PlaylistComment;

@Repository
public interface PlaylistCommentRepository extends CrudRepository<PlaylistComment, Long> { }
