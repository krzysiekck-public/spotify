package pl.softivo.spotify;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class SpotifyConsoleApplication implements CommandLineRunner {

    private static Logger LOG = LoggerFactory.getLogger(SpotifyConsoleApplication.class);

    public static void main(String[] args) {
        LOG.info("Spotify console application started!");
        new SpringApplicationBuilder(SpotifyConsoleApplication.class)
                .web(WebApplicationType.NONE)
                .run(args);
        LOG.info("Spotify console application finished!");
    }

    @Override
    public void run(String... args) throws Exception {
    }

}
